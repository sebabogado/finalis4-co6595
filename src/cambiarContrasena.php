<?php
include '../backend/cambiarContrasena.php';
include '../backend/crudUsuarios.php';

session_start();
if (!isset($_SESSION['usuario_id'])) {
    header('Location: login.php');
    exit();
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $contrasena_actual = $_POST['contrasena_actual'];
    $nueva_contrasena = $_POST['nueva_contrasena'];
    $confirmar_contrasena = $_POST['confirmar_contrasena'];

    if ($nueva_contrasena === $confirmar_contrasena) {
        $usuario = getUsuario($_SESSION['usuario_id']);
        if (!password_verify($contrasena_actual, $usuario['contrasena'])) {
            $nueva_contrasena_hash = password_hash($nueva_contrasena, PASSWORD_DEFAULT);
            updateContrasena($_SESSION['usuario_id'], $nueva_contrasena_hash);
            $mensaje = 'Contraseña actualizada exitosamente';
            header('Location: login.php');
            exit();
        } else {
            $error = 'La contraseña actual es incorrecta';
        }
    } else {
        $error = 'Las contraseñas nuevas no coinciden';
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Cambiar Contraseña</title>
</head>
<body>
    <h2>Cambiar Contraseña</h2>

    <?php if (isset($mensaje)): ?>
    <p><?php echo $mensaje; ?></p>
    <?php endif; ?>

    <?php if (isset($error)): ?>
    <p><?php echo $error; ?></p>
    <?php endif; ?>

    <form method="post">
        <label for="contrasena_actual">Contraseña Actual:</label><br>
        <input type="password" id="contrasena_actual" name="contrasena_actual" required><br>
        <label for="nueva_contrasena">Nueva Contraseña:</label><br>
        <input type="password" id="nueva_contrasena" name="nueva_contrasena" required><br>
        <label for="confirmar_contrasena">Confirmar Nueva Contraseña:</label><br>
        <input type="password" id="confirmar_contrasena" name="confirmar_contrasena" required><br>
        <input type="submit" value="Cambiar Contraseña">
    </form>

</body>
</html>
