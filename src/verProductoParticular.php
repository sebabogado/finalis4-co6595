<?php
include '../backend/crudProductos.php';

$producto = null;
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $codigo = $_POST['codigo'];
    $producto = getProducto($codigo);
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Ver Producto</title>
</head>
<body>
    <h2>Ver Producto</h2>

    <form method="post">
        <label for="codigo">Código:</label><br>
        <input type="text" id="codigo" name="codigo"><br>
        <input type="submit" value="Buscar Producto">
    </form>

    <?php if ($_SERVER['REQUEST_METHOD'] === 'POST'): ?>
        <?php if ($producto): ?>
            <p>Nombre: <?php echo $producto['nombre']; ?></p>
            <p>Código: <?php echo $producto['codigo']; ?></p>
            <p>Marca: <?php echo $producto['marca']; ?></p>
            <p>Precio: <?php echo $producto['precio']; ?></p>
        <?php else: ?>
            <p>No se encontró el producto con el código <?php echo $codigo; ?>.</p>
        <?php endif; ?>
    <?php endif; ?>

</body>
</html>
