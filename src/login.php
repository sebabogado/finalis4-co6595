<?php
    include '../backend/login.php';
?>

<!DOCTYPE html>
<html>
<head>
    <title>Iniciar sesión</title>
</head>
<body>
    <h2>Iniciar sesión</h2>

    <?php if (isset($error)): ?>
    <p><?php echo $error; ?></p>
    <p><?php echo $error2; ?></p>
    <p><?php echo $error3; ?></p>
    <p><?php echo $error4; ?></p>
    <?php endif; ?>

    <form method="post">
        <label for="correo_electronico">Correo electrónico:</label><br>
        <input type="email" id="correo_electronico" name="correo_electronico" required><br>
        <label for="contrasena">Contraseña:</label><br>
        <input type="password" id="contrasena" name="contrasena" required><br>
        <input type="submit" name="login" value="Iniciar sesión">
    </form>



</body>
</html>
