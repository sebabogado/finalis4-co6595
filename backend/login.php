<?php
    
    $dbh = new PDO('pgsql:host=localhost;dbname=postgres', 'postgres', 'postgres');

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['login'])) {
            $correo_electronico = $_POST['correo_electronico'];
            $contrasena = $_POST['contrasena'];

            $stmt = $dbh->prepare("SELECT * FROM usuarios WHERE correo_electronico = :correo_electronico");
            $stmt->bindParam(':correo_electronico', $correo_electronico);
            $stmt->execute();
            $usuario = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($usuario && !password_verify($contrasena, $usuario['contrasena'])) {
                session_start();
                $_SESSION['usuario_id'] = $usuario['usuario_id'];
                header('Location: ../src/verProductos.php');
                exit();
            } else {
                $error = 'Correo electrónico o contraseña incorrectos';
            }
        }
    }
?>
