<?php
include '../backend/crudProductos.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['delete'])) {
        $id = $_POST['id'];
        deleteProducto($id);
    }
    if (isset($_POST['update'])) {
        $id = $_POST['id'];
        $nombre = $_POST['nombre'];
        $codigo = $_POST['codigo'];
        $marca = $_POST['marca'];
        $precio = $_POST['precio'];
        updateProducto($id, $nombre, $codigo, $marca, $precio);
    }
}

$productos = getProductos();
?>

<!DOCTYPE html>
<html>
<head>
    <title>Listado de Productos</title>
    <style>
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            padding: 8px;
            text-align: left;
            border-bottom: 1px solid #ddd;
        }
    </style>
</head>
<body>
    <h2>Listado de Productos</h2>

    <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Buscar por nombre..">

    <table id="myTable">
        <tr>
            <th>Nombre</th>
            <th>Código</th>
            <th>Marca</th>
            <th>Precio</th>
            <th>Acciones</th>
        </tr>
        <?php foreach ($productos as $producto): ?>
        <tr>
            <td><?php echo $producto['nombre']; ?></td>
            <td><?php echo $producto['codigo']; ?></td>
            <td><?php echo $producto['marca']; ?></td>
            <td><?php echo $producto['precio']; ?></td>
            <td>
                <form method="post">
                    <input type="hidden" name="id" value="<?php echo $producto['producto_id']; ?>">
                    <input type="text" name="nombre" value="<?php echo $producto['nombre']; ?>">
                    <input type="text" name="codigo" value="<?php echo $producto['codigo']; ?>">
                    <input type="text" name="marca" value="<?php echo $producto['marca']; ?>">
                    <input type="number" name="precio" value="<?php echo $producto['precio']; ?>">
                    <input type="submit" name="update" value="Modificar">
                    <input type="submit" name="delete" value="Eliminar">
                </form>
            </td>
        </tr>
        <?php endforeach; ?>
    </table>
    
    <button onclick="location.href='crearProducto.php'">Crear Producto</button>
    <button onclick="location.href='verProductoParticular.php'">Ver un Producto en Particular </button>
    <button onclick="location.href='cambiarContrasena.php'">Cambiar contraseña </button>
    <button onclick="location.href='../backend/logout.php'">Cerrar Sesion </button>


    <script>
    function myFunction() {
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }       
        }
    }
    </script>

</body>
</html>
