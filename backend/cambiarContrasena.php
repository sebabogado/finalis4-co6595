<?php
    $dbh = new PDO('pgsql:host=localhost;dbname=postgres', 'postgres', 'postgres');

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['updateContrasena'])) {
            $usuario_id = $_POST['usuario_id'];
            $nueva_contrasena = $_POST['nueva_contrasena'];

            $nueva_contrasena_hash = password_hash($nueva_contrasena, PASSWORD_DEFAULT);

            $stmt = $dbh->prepare("UPDATE usuarios SET contrasena = :contrasena WHERE usuario_id = :usuario_id");
            $stmt->bindParam(':contrasena', $nueva_contrasena_hash);
            $stmt->bindParam(':usuario_id', $usuario_id);
            $stmt->execute();
        }
    }
?>
