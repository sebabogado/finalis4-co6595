<?php

    // Conexión a la base de datos
    $dbh = new PDO('pgsql:host=localhost;dbname=postgres', 'postgres', 'postgres');

    // Usuarios
    function getUsuarios() {
        global $dbh;
        $stmt = $dbh->prepare("SELECT * FROM usuarios");
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function getUsuario($id) {
        global $dbh;
        $stmt = $dbh->prepare("SELECT * FROM usuarios WHERE usuario_id = :id");
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function createUsuario($nombre, $apellido, $correo_electronico, $contrasena) {
        global $dbh;
        $stmt = $dbh->prepare("INSERT INTO usuarios (nombre, apellido, correo_electronico, contrasena) VALUES (:nombre, :apellido, :correo_electronico, :contrasena)");
        $stmt->bindParam(':nombre', $nombre);
        $stmt->bindParam(':apellido', $apellido);
        $stmt->bindParam(':correo_electronico', $correo_electronico);
        $stmt->bindParam(':contrasena', $contrasena);
        $stmt->execute();
        return $dbh->lastInsertId('usuarios_usuario_id_seq');
    }

    function updateUsuario($id, $nombre, $apellido, $correo_electronico, $contrasena) {
        global $dbh;
        $stmt = $dbh->prepare("UPDATE usuarios SET nombre = :nombre, apellido = :apellido, correo_electronico = :correo_electronico, contrasena = :contrasena WHERE usuario_id = :id");
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':nombre', $nombre);
        $stmt->bindParam(':apellido', $apellido);
        $stmt->bindParam(':correo_electronico', $correo_electronico);
        $stmt->bindParam(':contrasena', $contrasena);
        $stmt->execute();
    }

    function deleteUsuario($id) {
        global $dbh;
        $stmt = $dbh->prepare("DELETE FROM usuarios WHERE usuario_id = :id");
        $stmt->bindParam(':id', $id);
        $stmt->execute();
    }

?>
