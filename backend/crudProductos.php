<?php

    $dbh = new PDO('pgsql:host=localhost;dbname=postgres', 'postgres', 'postgres');
    // Productos
    function getProductos() {
        global $dbh;
        $stmt = $dbh->prepare("SELECT * FROM productos");
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function getProducto($id) {
        global $dbh;
        $stmt = $dbh->prepare("SELECT * FROM productos WHERE codigo = :id");
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function createProducto($nombre, $codigo, $marca, $precio) {
        global $dbh;
        $stmt = $dbh->prepare("INSERT INTO productos (nombre, codigo, marca, precio) VALUES (:nombre, :codigo, :marca, :precio)");
        $stmt->bindParam(':nombre', $nombre);
        $stmt->bindParam(':codigo', $codigo);
        $stmt->bindParam(':marca', $marca);
        $stmt->bindParam(':precio', $precio);
        $stmt->execute();
        return $dbh->lastInsertId('productos_producto_id_seq');
    }

    function updateProducto($id, $nombre, $codigo, $marca, $precio) {
        global $dbh;
        $stmt = $dbh->prepare("UPDATE productos SET nombre = :nombre, codigo = :codigo, marca = :marca, precio = :precio WHERE producto_id = :id");
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':nombre', $nombre);
        $stmt->bindParam(':codigo', $codigo);
        $stmt->bindParam(':marca', $marca);
        $stmt->bindParam(':precio', $precio);
        $stmt->execute();
    }

    function deleteProducto($id) {
        global $dbh;
        $stmt = $dbh->prepare("DELETE FROM productos WHERE producto_id = :id");
        $stmt->bindParam(':id', $id);
        $stmt->execute();
    }

?>
