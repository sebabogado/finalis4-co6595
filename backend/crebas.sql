BEGIN;

CREATE TABLE IF NOT EXISTS public.usuarios
(
    usuario_id bigserial NOT NULL,
    nombre character varying(50) COLLATE pg_catalog."default",
    apellido character varying(50) COLLATE pg_catalog."default",
    correo_electronico character varying(100) COLLATE pg_catalog."default",
    contrasena character varying(100) COLLATE pg_catalog."default",
    CONSTRAINT usuarios_pkey PRIMARY KEY (usuario_id),
    CONSTRAINT usuarios_correo_electronico_key UNIQUE (correo_electronico)
);

CREATE TABLE IF NOT EXISTS public.productos
(
    producto_id bigserial NOT NULL,
    nombre character varying(50) COLLATE pg_catalog."default",
    codigo character varying(50) COLLATE pg_catalog."default",
    marca character varying(100) COLLATE pg_catalog."default",
    precio int,
    CONSTRAINT productos_pkey PRIMARY KEY (producto_id)
);

COMMIT;
