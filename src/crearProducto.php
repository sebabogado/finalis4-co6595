<?php
include '../backend/crudProductos.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $nombre = $_POST['nombre'];
    $codigo = $_POST['codigo'];
    $marca = $_POST['marca'];
    $precio = $_POST['precio'];

    createProducto($nombre, $codigo, $marca, $precio);
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Crear Producto</title>
</head>
<body>
    <h2>Crear Producto</h2>

    <form method="post">
        <label for="nombre">Nombre:</label><br>
        <input type="text" id="nombre" name="nombre"><br>
        <label for="codigo">Código:</label><br>
        <input type="text" id="codigo" name="codigo"><br>
        <label for="marca">Marca:</label><br>
        <input type="text" id="marca" name="marca"><br>
        <label for="precio">Precio:</label><br>
        <input type="number" id="precio" name="precio"><br>
        <input type="submit" value="Crear Producto">
    </form>

</body>
</html>
