<?php
    session_start();

    if (isset($_SESSION['usuario_id'])) {
        unset($_SESSION['usuario_id']);
        session_destroy();
        header('Location: ../src/login.php');
        exit();
    } else {
        header('Location: ../src/login.php');
        exit();
    }
?>
